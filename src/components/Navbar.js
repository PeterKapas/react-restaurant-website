import Logo from '../assets/pizzaLogo.png'
import { Link } from 'react-router-dom'
import ReorderIcon from '@mui/icons-material/Reorder'
import '../styles/Navbar.css'
import { useState } from 'react'

const Navbar = () => {
	const [openLinks, setOpenLinks] = useState(false)
	const toggleNavbar = () => {
		setOpenLinks((prevOpenLinks) => !prevOpenLinks)
	}
	return (
		<div className='navbar'>
			<div className='leftside' id={openLinks ? 'open' : 'close'}>
				<Link to='/'>
					<img src={Logo} alt='pizzaLogo' />
				</Link>

				<div className='hiddenLinks'>
					<Link to='/'>Home</Link>
					<Link to='/menu'>Menu</Link>
					<Link to='/about'>About</Link>
					<Link to='/contact'>Contact</Link>
				</div>
			</div>
			<div className='rightside'>
				<Link to='/'>Home</Link>
				<Link to='/menu'>Menu</Link>
				<Link to='/about'>About</Link>
				<Link to='/contact'>Contact</Link>
				<button onClick={toggleNavbar}>
					<ReorderIcon />
				</button>
			</div>
		</div>
	)
}
export default Navbar
