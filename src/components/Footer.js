import { Link } from 'react-router-dom'
import InstagramIcon from '@mui/icons-material/Instagram'
import TwitterIcon from '@mui/icons-material/Twitter'
import FacebookIcon from '@mui/icons-material/Facebook'
import LinkedInIcon from '@mui/icons-material/LinkedIn'
import '../styles/Footer.css'
import { AiFillGitlab } from 'react-icons/ai'

const Footer = () => {
	return (
		<div className='footer'>
			<div className='socialMedia'>
				<Link to='notfound'>
					<InstagramIcon />
				</Link>
				<a href='https://gitlab.com/PeterKapas' target='_blank'>
					<AiFillGitlab />
				</a>
				<Link to='notfound'>
					<FacebookIcon />
				</Link>

				<a
					href='https://www.linkedin.com/in/p%C3%A9ter-kap%C3%A1s-32689912b/'
					target='_blank'
				>
					<LinkedInIcon />
				</a>
			</div>
			<p> Happy Pizza Day (applies to every day of the year)</p>
			<p> &copy; 2022 peterspizzaszeged.com </p>
		</div>
	)
}
export default Footer
