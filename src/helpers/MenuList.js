import Pepperoni from '../assets/pepperoni.jpg'
import Margherita from '../assets/margherita.jpg'
import PetersSpecial from '../assets/petersspecial.jpg'
import Vegan from '../assets/vegan.jpg'
import Pineapple from '../assets/pineapple.jpg'
import Expensive from '../assets/expensive.jpg'

export const MenuList = [
	{
		name: 'Pepperoni Pizza',
		image: Pepperoni,
		price: 15.99,
	},
	{
		name: 'Margherita Pizza',
		image: Margherita,
		price: 11.99,
	},
	{
		name: "Peter's Special Pizza",
		image: PetersSpecial,
		price: 16.99,
	},
	{
		name: 'Vegan Pizza',
		image: Vegan,
		price: 17.99,
	},
	{
		name: 'Pineapple Pizza',
		image: Pineapple,
		price: 14.99,
	},
	{
		name: '6 wishes pizza',
		image: Expensive,
		price: 19.99,
	},
]
