import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Home from './pages/Home'
import Menu from './pages/Menu'
import About from './pages/About'
import Contact from './pages/Contact'
import NotFound from './pages/NotFound'

const App = () => {
	return (
		<div className='App'>
			<Router>
				<Navbar />
				<Routes>
					<Route exact path='/' element={<Home />} />
					<Route exact path='/menu' element={<Menu />} />
					<Route exact path='/about' element={<About />} />
					<Route exact path='/Contact' element={<Contact />} />
					<Route exact path='*' element={<NotFound />} />
				</Routes>
				<Footer />
			</Router>
		</div>
	)
}
export default App
