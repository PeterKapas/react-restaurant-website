import pizzaLeft1 from '../assets/pizzaLeft1.jpg'
import '../styles/Contact.css'

const Contact = () => {
	return (
		<div className='contact'>
			<div
				className='leftSide'
				style={{ backgroundImage: `url(${pizzaLeft1})` }}
			></div>
			<div className='rightSide'>
				<h1>Contact Us</h1>
				<form id='contact-form'>
					<label htmlFor='Full Name'>Full Name</label>
					<input name='name' placeholder='Enter full name' type='text'></input>
					<label htmlFor='Email'>Email</label>
					<input name='email' placeholder='Enter email' type='email' />
					<label htmlFor='message'>Message</label>
					<textarea
						name='message'
						rows='6'
						placeholder='Enter message...'
						required
					></textarea>
					<button type='submit'>Send message</button>
				</form>
			</div>
		</div>
	)
}
export default Contact
