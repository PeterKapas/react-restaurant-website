import pizza from '../assets/pizza.jpeg'
import '../styles/NotFound.css'
import { Link } from 'react-router-dom'

const NotFound = () => {
	return (
		<div className='not-found' style={{ backgroundImage: `url(${pizza})` }}>
			<h1>Page not found</h1>
			<p>Would you rather have a pizza than see this ugly 404 message?</p>
			<Link to='/menu'>
				<button> Show me the pizzas</button>
			</Link>
		</div>
	)
}
export default NotFound
