import { Link } from 'react-router-dom'
import BannerImage from '../assets/pizza1.jpg'
import '../styles/Home.css'

const Home = () => {
	return (
		<div className='home' style={{ backgroundImage: `url(${BannerImage})` }}>
			<div className='headerContainer'>
				<h1>Peter's Pizza</h1>
				<p id='break1'>Probably the best </p>
				<p id='break2'> Pizza in town...</p>
				<Link to='/menu'>
					<button>Order Now</button>
				</Link>
			</div>
		</div>
	)
}
export default Home
