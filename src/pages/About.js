import multiplePizzas1 from '../assets/multiplePizzas1.jpg'
import '../styles/About.css'

const About = () => {
	return (
		<div className='about'>
			<div
				className='aboutTop'
				style={{ backgroundImage: `url(${multiplePizzas1})` }}
			></div>
			<div className='aboutBottom'>
				<h1>About Us</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident
					dolores distinctio molestias? Dicta cum at tempore ipsum quia!
					Aliquam, ipsa et eaque aut minus incidunt nisi corrupti hic dolor
					commodi voluptas tempora blanditiis ut consectetur autem, est deserunt
					molestias ea soluta quam? Veritatis vitae odit voluptate molestiae
					sunt rem veniam modi sint corrupti eveniet. Iste sapiente suscipit
					labore eius! Fuga sint at harum molestias alias illum veritatis enim
					minima tenetur sapiente quasi nostrum, voluptatum odit deserunt
					consequuntur facilis quisquam, expedita dolor a. Delectus, ut nesciunt
					omnis quod, pariatur ratione, enim unde cupiditate harum esse
					temporibus voluptatum minus ea provident mollitia!
				</p>
			</div>
		</div>
	)
}
export default About
